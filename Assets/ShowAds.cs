﻿using UnityEngine;
using System.Collections;
using Heyzap;

public class ShowAds : MonoBehaviour {

    public static ShowAds instance = null;

    void Awake()
    {

        if (instance == null)

            instance = this;

        else if (instance != this)
            //Destroy this, this enforces our singleton pattern so there can only be one instance of SoundManager.
            Destroy(gameObject);

        //Set SoundManager to DontDestroyOnLoad so that it won't be destroyed when reloading our scene.
        DontDestroyOnLoad(gameObject);
    }

//    // Use this for initialization
    void Start()
    {
        HeyzapAds.Start("8f25340136433f655e3493f5c1618ad9", HeyzapAds.FLAG_NO_OPTIONS);

        HZInterstitialAd.Fetch();
#if UNITY_ANDROID
        HZBannerShowOptions showOptions = new HZBannerShowOptions();
        showOptions.Position = HZBannerShowOptions.POSITION_TOP;
        HZBannerAd.ShowWithOptions(showOptions);
#endif
 
     //   ShowInter();
        ShowBanner();
    }



    public void ShowInter()
    {
        StartCoroutine(ShowInterstecialAd());
    }

    public void FetchVideo()
    {
        HZVideoAd.Fetch();
    }
    public void ShowBanner()
    {
        HZBannerShowOptions showOptions = new HZBannerShowOptions();
        showOptions.Position = HZBannerShowOptions.POSITION_TOP;
        HZBannerAd.ShowWithOptions(showOptions);
    }
    public void ShowVideoAds()
    {
        StartCoroutine(ShowVideoAdsCoro());
    }
    IEnumerator ShowVideoAdsCoro()
    {
        yield return new WaitForSeconds(3f);
        if (HZVideoAd.IsAvailable())
        {
            HZVideoAd.Show();
        }
    }
    IEnumerator ShowInterstecialAd()
    {
        yield return new WaitForSeconds(3f);

        if (HZInterstitialAd.IsAvailable())
        {
            HZInterstitialAd.Show();
        }
    }


    public void ShowMediationSuite()
    {
         HeyzapAds.ShowMediationTestSuite();   
    }
}
